const express = require('express');
const socket = require('socket.io');

const app = express();

app.get('/', (req, res) => {
  res.json({
    message: 'Ok_1612387734490',
  });
});

const server = app.listen(process.env.PORT || 4000, () => {
  console.log('Listening on port ', process.env.PORT || 4000);
});

const io = socket(server, {
  cors: {
    origin: '*',
  },
});

const roomsCollection = {};

io.on('connection', (socket) => {
  console.log('User connected: ' + socket.id);
  socket.emit('connected', socket.id);

  socket.on(
    'create-to-server',
    ({ roomName, roomToken, userName, maxNumber }) => {
      const rooms = io.sockets.adapter.rooms;

      if (roomsCollection[roomToken]) {
        socket.emit('roomAlreadyCreated');
        return;
      }

      roomsCollection[roomToken] = {
        name: roomName,
        createdBy: userName,
        maxNumberOfGuests: maxNumber,
        chatHistory: [],
      };

      socket.join(roomToken);
      socket.emit('created');
    }
  );

  socket.on('join-to-server', ({ roomToken }) => {
    const rooms = io.sockets.adapter.rooms;
    const room = rooms.get(roomToken);
    const roomConfig = roomsCollection[roomToken];

    if (!roomConfig || !room) {
      socket.emit('noSuchRoom');
      return;
    }

    if (room.size === roomConfig.maxNumberOfGuests) {
      socket.emit('full');
      return;
    }

    socket.join(roomToken);
    socket.emit('joined', roomConfig);
  });

  socket.on(
    'ready-to-server',
    (roomToken, fromId, userName, isVideoHidden, isAudioMuted) => {
      console.log('Ready', roomToken, fromId, userName);
      socket.broadcast
        .to(roomToken)
        .emit('ready', fromId, userName, isVideoHidden, isAudioMuted);
    }
  );

  socket.on('candidate-to-server', (candidate, roomToken, fromId, toId) => {
    console.log('Candidate', candidate);
    socket.broadcast.to(roomToken).emit('candidate', candidate, fromId, toId);
  });

  socket.on(
    'offer-to-server',
    (offer, roomToken, fromId, toId, userName, isVideoHidden, isAudioMuted) => {
      console.log('Offer', offer);

      socket.broadcast
        .to(roomToken)
        .emit(
          'offer',
          offer,
          fromId,
          toId,
          userName,
          isVideoHidden,
          isAudioMuted
        );
    }
  );

  socket.on('answer-to-server', (answer, roomToken, fromId, toId) => {
    console.log('Answer', answer);
    socket.broadcast.to(roomToken).emit('answer', answer, fromId, toId);
  });

  socket.on('leave-to-server', (roomToken, fromId) => {
    console.log('Leave', roomToken, fromId);
    socket.leave(roomToken);

    socket.broadcast.to(roomToken).emit('leave', fromId);

    const rooms = io.sockets.adapter.rooms;
    const room = rooms.get(roomToken);
    if (!room) {
      delete roomsCollection[roomToken];
    }
  });

  socket.on('text-to-server', (message, fromId, roomToken) => {
    console.log('Text message', message, fromId, roomToken);

    const roomConfig = roomsCollection[roomToken];

    roomConfig.chatHistory.push(message);
    io.sockets.in(roomToken).emit('chat-message', message, fromId);
  });

  socket.on('user-audio-toggled-to-server', (roomToken, fromId, isEnabled) => {
    console.log('User audio toggled', roomToken, fromId, isEnabled);

    io.sockets.in(roomToken).emit('user-audio-toggled', fromId, isEnabled);
  });

  socket.on('user-video-toggled-to-server', (roomToken, fromId, isEnabled) => {
    console.log('User video toggled', roomToken, fromId, isEnabled);

    io.sockets.in(roomToken).emit('user-video-toggled', fromId, isEnabled);
  });
});
